var express = require('express');
var router = express.Router();
const db = require("../db/functions");
const dotenv = require('dotenv');
dotenv.config();

router.get('/', async function (req, res, next) {
  res.render('pages/fringe-live', {isLoggedIn: req.session.authorised});
});

// router.get('/live', async function (req, res, next) {
//   res.render('pages/fringe-live', {isLoggedIn: req.session.authorised});
// });

router.get('/privacy-policy', async function (req, res, next) {
  res.render('pages/privacy-policy', {isLoggedIn: req.session.authorised});
});


router.post('/mailing-list', async function (req, res, next) {
  var result = await db.register(req.body.email);
  res.send();
});

module.exports = router;