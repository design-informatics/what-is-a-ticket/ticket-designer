var express = require('express');
var router = express.Router();
const db = require("../db/functions");
const data = require('../data/data.json'); //with path

/* GET my account page. */
router.get('/', async function(req, res, next) {
  var tickets = await db.getTickets(req.session.userid);
  res.render('pages/account', {isLoggedIn: req.session.authorised, tickets: tickets});
});

/* logout */
router.get('/logout', function(req, res, next) {
  if (req.session) {
    // delete session object
    req.session.destroy(function(err) {
      if(err) {
        return next(err);
      } else {
        return res.redirect('/');
      }
    });
  }
});

router.post('/delete', async function(req, res, next) {
  if (!req.session.authorised) {
    return res.redirect('/');
  }

  var result = await db.deleteUser(req.session.userid);
  if (!result) {
    res.status(400).send({message: 'Account could not be deleted'});
    return;
  }

  return res.send()
  
});

/* GET design page. */
router.get('/design', async function(req, res, next) {
  res.render('pages/design', {data: data, isLoggedIn: req.session.authorised});
});



module.exports = router;