var express = require('express');
var router = express.Router();
const db = require("../db/functions");

/* POST new ticket. */
router.post('/ticket', async function(req, res, next) {
  var fail = false
  var ticket_data = req.body.data ? req.body.data : fail = true;

  if (fail) {
    res.status(400).send({
      message: "No ticket data found in request."
    });
    return;
  }

  ticket_data = JSON.parse(ticket_data);
  var result = await db.addTicket(ticket_data, req.session.userid);
  var tickets = await db.getTickets(req.session.userid);

  if (result != false) {
    res.send(JSON.stringify(tickets));
  }
  else {
    res.status(500).send({
      message:
        err.message || "Some error occurred while saving the ticket."
    });
  }


});


/* GET ticket. */
router.get('/ticket', async function(req, res, next) {
  var tickets = await db.getTickets(req.session.userid);

  if (tickets != false) {
    res.send(JSON.stringify(tickets));
  }
  else {
    res.status(500).send({
      message:
        "Some error occurred while getting tickets"
    });
  }
});

router.post('/delete-ticket', async function(req, res, next) {

  var result = await db.deleteTicket(req.body.id, req.session.userid);
  if (!result) {
    res.status(400).send({message: 'Ticket could not be deleted'});
    return;
  }

  return res.send()
  
});



module.exports = router;