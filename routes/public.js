var express = require('express');
var router = express.Router();
const db = require("../db/functions");
const fs = require('fs');

router.get('/', async function(req, res, next) {
  res.render('pages/home', {isLoggedIn: req.session.authorised});
});

router.get('/login', async function(req, res, next) {
  res.render('pages/login', {isLoggedIn: req.session.authorised});
});

router.get('/about', async function(req, res, next) {
  res.render('pages/about', {isLoggedIn: req.session.authorised});
});

router.get('/faq', async function(req, res, next) {
  res.render('pages/faq', {isLoggedIn: req.session.authorised});
});

router.get('/privacy-policy', async function (req, res, next) {
  res.render('pages/privacy-policy', {isLoggedIn: req.session.authorised});
});

router.get('/info-sheet', async function (req, res, next) {
  const info_sheet = `./public/files/Ticket Designer Website - Information Sheet.pdf`;
  res.download(info_sheet);
});

/* GET explore page. */
router.get('/explore', async function(req, res, next) {
  res.render('pages/explore', {isLoggedIn: req.session.authorised});
});

router.get('/ticket/:ticketID', async function(req, res, next) {
  var ticket = await db.getTicket(req.params.ticketID);
  console.log(req.params.ticketID)
  if (ticket != false) {
    res.render('pages/ticket', {ticket: ticket, isLoggedIn: req.session.authorised});
  }
  else {
    res.status(500).send({
      message:
        "Some error occurred while getting ticket"
    });
  }
});

/* GET signup page. */
router.get('/signup', function(req, res, next) {
  if (req.session.authorised) {
    return res.redirect('/');
  }
  res.render('pages/signup', {isLoggedIn: req.session.authorised});
});

router.get('/randomticket', async function(req, res, next) {
  var seenTickets = JSON.parse(req.query.seen) || [];
  var ticket = await db.getRandomTicket(seenTickets);

  if (ticket != false) {
    res.send(JSON.stringify(ticket));
  }
  else {
    res.status(500).send({
      message:
        "Some error occurred while getting ticket"
    });
  }
});

router.post('/report', async function(req, res, next) {
  var fail = false
  var badTicket = req.body.id ? req.body.id: fail = true;

  if (fail) {
    res.status(400).send({
      message: "No ticket data found in request."
    });
    return;
  }

  var result = await db.reportTicket(badTicket);

  if (result != false) {
    res.send();
  }
  else {
    res.status(500).send({
      message:
        "Some error occurred while reporting the ticket."
    });
  }
});

router.post('/signup', async function(req, res, next) {
  if (req.session.authorised) {
    return res.redirect('/');
  }

  var exists = await db.checkUserExists(req.body.email);
  if (exists) {
    res.status(400).send('A user with this email already exists.');
    return;
  }

  let subscribe = req.body.contact == "on" ? true : false;
  db_user = await db.createUser(null, req.body.email, req.body.password, false, subscribe);

  if (db_user != null && db_user != false) {
    req.session.authorised = true;
    req.session.admin = false;
    req.session.userid = db_user.id;
    req.session.username = null;
  }

  res.send();
  
});

module.exports = router;