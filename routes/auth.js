var express = require('express');
var router = express.Router();
const db = require("../db/functions");

/* authenticate */
router.post('/', async function(req, res, next) {
  db_user = await db.getUser(req.body.email, req.body.password);

  if (db_user != null && db_user != false) {
    req.session.authorised = true;
    req.session.admin = db_user.admin;
    req.session.userid = db_user.id;
    req.session.email = db_user.email;
    res.send();
  }
  else {
    if (req.rateLimit.remaining === 0) {
      res.status(429).send();  
    }
    else {
      res.status(406).json({remaining: req.rateLimit.remaining});  
    }  
  }
  
});

module.exports = router;
