var express = require('express');
var router = express.Router();
const db = require("../db/functions");
const ObjectsToCsv = require('objects-to-csv');
const csv=require('csvtojson')


router.get('/', async function (req, res, next) {
  var mailing_list = await db.getML();
  var users = await db.getUsers();
  var tickets = await db.getAllTickets(['user']);
  var email_list = [];
  var user_list = [];
  if (mailing_list != false) { 
    for (const [key, value] of Object.entries(mailing_list)) {
      email_list.push(value.email);
    }
  }

  if (users != false) { 
    for (const [key, value] of Object.entries(users)) {
      user_list.push(value.email);
    }
  }


  var uniqueEmailList = [...new Set(email_list)]
  var uniqueUserlList = [...new Set(user_list)]
  
  res.render('pages/admin', {
    ml: uniqueEmailList,
    users: uniqueUserlList,
    count: uniqueUserlList.length,
    tickets: tickets,
    isLoggedIn: req.session.authorised
  });
});

// router.get('/restore', async function(req, res, next) {
//   const jsonArray = await csv().fromFile('./db/backup.csv');
//   console.log(jsonArray);
//   jsonArray.forEach(async ticket => {
//     await db.restoreTicket(ticket);
//   })
//   res.send();
// });

/* Download tickets. */
router.get('/db/tickets', async function(req, res, next) {
  var tickets = await db.getAllTickets(['']);

  const csv = new ObjectsToCsv(tickets);
  var dateString = new Date().toLocaleDateString('en-GB').replace(/\//g,'-');
  var filename = './db/dumps/' + 'tickets_' + dateString + '.csv'; 
  // Save to file:
  await csv.toDisk(filename);

  if (tickets != false) {
    res.download(filename);
  }
  else {
    res.status(500).send({
      message:
        "Some error occurred while fetching file"
    });
  }
});

/* Download parts. */
router.get('/db/parts', async function(req, res, next) {
  var parts = await db.getParts();
  console.log(parts)
  const csv = new ObjectsToCsv(parts);
  var dateString = new Date().toLocaleDateString('en-GB').replace(/\//g,'-');
  var filename = './db/dumps/' + 'parts_' + dateString + '.csv'; 
 
  // Save to file:
  await csv.toDisk(filename);

  if (parts != false) {
    res.download(filename);
  }
  else {
    res.status(500).send({
      message:
        "Some error occurred while fetching file"
    });
  }
});

module.exports = router;