(function () {
  'use strict';
  window.addEventListener('load', function () {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.querySelectorAll('.needs-validation')

    // Loop over them and prevent submission
    Array.prototype.slice.call(forms)
      .forEach(function (form) {
        form.addEventListener('submit', function (event) {
          if (!form.checkValidity()) {
            event.preventDefault()
            event.stopPropagation()
            if (!$('#invalidCheck').is(":checked")) {
              $("#checkFeedback").show();
            } else {
              $("#checkFeedback").hide();
            }

          } else {
            submitSignup(event);
          }

          form.classList.add('was-validated')
        }, false)
      })

    document.getElementById("inputPassword2").addEventListener("input", checkPasswords);

  }, false);

})();

function checkPasswords() {
  console.log($("#invalidCheck").val());
  if ($("#inputPassword2").val() != $("#inputPassword").val()) {
    $("#inputPassword2").addClass('is-invalid');
  } else {
    $("#inputPassword2").removeClass('is-invalid');
  }
}

function submitSignup(e) {
  e.preventDefault();
  $('#signupError').hide();
  $("#signupSubmit").hide();
  $("#signupSpinner").show();
  $.post("/signup", $("#signupform").serialize(), function (data) {
      return data;
    }).then(data => {
      location.replace("/account/design");
    })
    .fail(data => {
      console.log(data);
      $("#signupSpinner").hide();
      $("#signupSubmit").show();
      $('#signupError').text(data.responseText);
      $('#signupError').show();
      $('#signupFail').show();
    })
}