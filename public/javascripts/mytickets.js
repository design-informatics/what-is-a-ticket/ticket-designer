$(document).ready(function () {
    $('.btn-delete').on('click', function () {
        deleteTicket(this.dataset.id);
    })
});

function deleteTicket(id) {
    $.post("/api/delete-ticket", {
            id: id
        }, function (data) {
            $('#ticket' + id).remove();
        })
        .fail(function (data) {
            console.log(data.responseJSON.message);
        })
}

$('#deleteAccount').on('click', () => {
    $('#deleteAccount').hide()
    $("#deleteSpinner").show();

    $.post("/account/delete", function (data) {
            setTimeout(function () {
                $("#deleteSpinner").hide();
            }, 1000);
            location.replace("/account/logout");
        })
        .fail(function (data) {
            console.log(data.responseJSON.message);
        })
})