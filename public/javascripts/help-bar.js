$('#reportButton').on('click', function () {
    let selected = $('#helpBar').data('ticket')
    var badTicket = $('#ticketID' + selected ).data('id');
    $.post("/report", {
            id: badTicket
        }, function (data) {
            $(".ticket"+selected+" #conditionText").text('** REPORTED **');
            $(".ticket"+selected+" #actionText").text('** REPORTED **');
            $(".ticket"+selected+" #eventText").text('** REPORTED **');
        })
        .fail(function (data) {
            console.log(data.responseJSON.message);
        })
    $('#reportModal').modal('hide');
})

$('#shareButton').on('click', function () {
    let selected = $('#helpBar').data('ticket')
    var shareTicket = $('#ticketID' + selected ).data('id');
    $('#ticketLink:text').val(window.location.origin+'/ticket/'+shareTicket)
})

$('#shareCopyButton').on('click', function () {
    navigator.clipboard.writeText($('#ticketLink:text').val());
    $(this).hide();
    $('#shareCopyButtonDone').show();
})

$('#downloadButton').on('click', function () {
    downloadTicket();
})



// TODO ticket download option
function downloadTicket() {
    var ticket = document.getElementsByClassName("ticket-container")[0];
    html2canvas(ticket).then(function(canvas) {
        var img    = canvas.toDataURL("image/png");
        const link = document.createElement('a')
        link.href = img;
        link.download = 'My Ticket'
        document.body.appendChild(link)
        link.click()
        document.body.removeChild(link)
    });
}