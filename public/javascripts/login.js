(function() {
    'use strict';
    window.addEventListener('load', function() {
      // Fetch all the forms we want to apply custom Bootstrap validation styles to
      var forms = document.getElementsByClassName('needs-validation');
      // Loop over them and prevent submission
      var validation = Array.prototype.filter.call(forms, function(form) {
        form.addEventListener('submit', function(event) {
          if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
          }
          form.classList.add('was-validated');
        }, false);
      });
    }, false);
    
    document.getElementById("loginform").addEventListener("submit", submitLogin);
  })();

function submitLogin(e) {
    e.preventDefault();
    $( '#loginError' ).hide();
    if ($( "#inputEmail" ).val() && $( "#inputPassword" ).val()) {
        $( "#loginSubmit" ).hide();
        $( "#loginSpinner" ).show();
        $.post( "/auth", $( "#loginform" ).serialize(), function( data ) {
            return data;
        }).then(data => {       
            location.replace("/");
          })
          .fail(data => {
            console.log(data);
            if (data.status === 429) {
              $( '#loginFail' ).hide();
              $( '#loginLimit' ).show();
              $( "#loginSpinner" ).hide();
            } 
            else {
              $( "#loginSpinner" ).hide();
              $( "#loginSubmit" ).show();
              $( '#loginFail' ).show();
            }
          })
        }       
  }