const ticket_raw = document.getElementById("data");
const ticket = JSON.parse(ticket_raw.dataset.ticket);

$(document).ready(function () {
    applyToTicket();
});

function pad(str, max) {
    str = str.toString();
    return str.length < max ? pad("0" + str, max) : str;
}

function strip(html){
    let doc = new DOMParser().parseFromString(html, 'text/html');
    return doc.body.textContent || "";
 }

function applyToTicket() {
    console.log(ticket)
    if (strip(ticket.condition_text).split(' ')[0].toLowerCase() == 'if') {
        $("#conditionText").html(ticket.condition_text);
    }
    else {
        $("#conditionText").html('if ' + ticket.condition_text);
    }
    $('#eventText').html(ticket.event_type);
    $('#actionText').html(ticket.action_text);
    $('#ticketIDText').text('TD' + pad(ticket.id, 4));

    let p = ['conditions', 'actions'];
    p.forEach((part) => {
        $(".category-icon-" + part).each(function() {
            if($(this).hasClass('category-icon-selected')){
                $(this).removeClass('category-icon-selected')
                $(this).addClass('category-icon-unselected')
                }
        });
        var icon = $('.category-icons :input[value="'+ ticket[part.slice(0, -1)+'_category']+'"]');
        icon.removeClass('category-icon-unselected');
        icon.addClass('category-icon-selected')
    })
}
