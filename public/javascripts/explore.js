$(document).ready(function () {
    applyToTicket(0);
});

var carousel = document.getElementById('carouselExplore')

carousel.addEventListener('slide.bs.carousel', (e) => {
    $('#helpBar').data('ticket', e.to)
    applyToTicket(e.to);
})

function pad(str, max) {
    str = str.toString();
    return str.length < max ? pad("0" + str, max) : str;
}

function strip(html){
    let doc = new DOMParser().parseFromString(html, 'text/html');
    return doc.body.textContent || "";
 }

function applyToTicket(n) {
    var ticket_data = false;
    var seenTickets = JSON.parse(sessionStorage.getItem("seenTickets")) || [];
    $.get("/randomticket", {seen: JSON.stringify(seenTickets)}, function (data) {
            ticket_data = JSON.parse(data);
            var random_ticket = ticket_data[Math.floor(Math.random() * ticket_data.length)]
            if (strip(random_ticket.condition_text).split(' ')[0].toLowerCase() == 'if') {
                $(".ticket"+n.toString()+" #conditionText").html(random_ticket.condition_text);
            }
            else {
                $(".ticket"+n.toString()+" #conditionText").html('if ' + random_ticket.condition_text);
            }
            
            $(".ticket"+n.toString()+" #eventText").html(random_ticket.event_type);
            $(".ticket"+n.toString()+" #actionText").html(random_ticket.action_text);
            $(".ticket"+n.toString()+" #ticketIDText").text('TD' + pad(random_ticket.id, 4));
            $('#ticketID' + n.toString()).data('id', random_ticket.id);
            let ticketID = parseInt(random_ticket.id);
            seenTickets.push(ticketID);
            seenTickets = [...new Set(seenTickets)]; // get unique ticket ids
            sessionStorage.setItem("seenTickets", JSON.stringify(seenTickets));
            let p = ['conditions', 'actions'];
            p.forEach((part) => {
                $(".ticket"+n.toString()+" .category-icon-" + part).each(function() {
                    if($(this).hasClass('category-icon-selected')){
                        $(this).removeClass('category-icon-selected')
                        $(this).addClass('category-icon-unselected')
                     }
                });
                var icon = $('.ticket'+n.toString()+ ' .category-icons :input[value="'+ random_ticket[part.slice(0, -1)+'_category']+'"]');
                icon.removeClass('category-icon-unselected');
                icon.addClass('category-icon-selected')
            })
        })
        .fail(function (data) {
            sessionStorage.setItem("seenTickets", JSON.stringify([]));
            $(".ticket"+n.toString()+" #conditionText").text('** no tickets found **');
            $(".ticket"+n.toString()+" #actionText").text('');
            $(".ticket"+n.toString()+" #ticketIDText" ).text('');
            $(".ticket"+n.toString()+" #eventText" ).text('');
            console.log(data.responseJSON.message);
        })
}