const data_raw = document.getElementById("data");
var categories = {conditions: '', actions: ''};

var introScreenNum = 1;
var helperVisible = true;
var introSeen = localStorage.getItem("introSeen");

$(document).ready(function() {  
    $(".templateSelect").select2({
        templateResult: formatState,
        templateSelection: formatState,
        minimumResultsForSearch: Infinity,
        placeholder: "Select a template...",
        theme: 'bootstrap4',
        selectionCssClass: 'form-select'
    });
    var element = document.getElementsByClassName("select2-selection--single");
    if (element.length > 0) {
        for(var i = 0; i <= element.length; i++) {
            element[0].classList.remove('select2-selection--single');
        };
    }

});

function strip(html){
    let doc = new DOMParser().parseFromString(html, 'text/html');
    return doc.body.textContent || "";
 }

$('.templateSelect').on('select2:select', function (e) {
    var data = e.params.data;
    let this_data = JSON.parse(data.id)
    let xyz = ['x', 'y', 'z'];
    let template_html = this_data.text;
    $('#'+e.target.id+'Div').hide();
    let template_type = e.target.id.replace('Select', '');
    $('#'+template_type+'Clear').show();
    $('#'+template_type+'Edit').show();
    var whatyoulike = false;
    
    xyz.forEach(r => {
        if(this_data.text.includes('%' + r)) {
            let replace_html = '';
            let var_type = this_data[r + '_type'];
            if( var_type == 'dropdown'){
                let datalist = document.createElement("datalist");
                datalist.id = r+'List-'+template_type;
                var values = this_data[r + '_values'].split(',');
                values.forEach(v => {
                    let template_option = document.createElement("option");
                    template_option.value = v.trim();
                    datalist.innerHTML += template_option.outerHTML;
                });
                
                replace_html = "<input type='text' placeholder='click to edit' class='template-part template-dropdown' id="+ "'"+r+"Value-"+ template_type+"' list="+ "'"+r+"List-"+template_type+"' autocomplete='off'>"+ datalist.outerHTML + '</input>';
            } else if(var_type == 'number') {
                replace_html = document.createElement("input");
                replace_html.id = r+"Value-"+ template_type;
                replace_html.classList.add('template-part');
                replace_html.classList.add('template-part-number');
                replace_html.setAttribute("autocomplete","off");
                replace_html.setAttribute("placeholder","click");
                replace_html.type = "number";
                replace_html = replace_html.outerHTML;      
            } else {          
                if(this_data.text == '%' + r) {
                    whatyoulike = true;
                    replace_html = document.createElement("textarea");
                    replace_html.classList.add('template-part-wide');
                }
                else {
                    replace_html = document.createElement("input");
                }
                replace_html.id = r+"Value-"+ template_type;
                replace_html.classList.add('template-part');

                replace_html.setAttribute("autocomplete","off");
                replace_html.setAttribute("placeholder","click to edit");
                replace_html.type = "text";
                replace_html = replace_html.outerHTML;
            }
            template_html = template_html.replace('%' + r, replace_html);     
        }
    });
    
    $('.'+ template_type + '-edit').html("");
    if (template_type == 'conditions' && !whatyoulike) {
        $('.conditions-edit').append('If ');
    }
    $('.'+ template_type + '-edit').append(template_html);


    var templateDropdown = document.getElementsByClassName("template-dropdown");
    for(var i = 0; i < templateDropdown.length; i++) {
        templateDropdown[i].addEventListener("focus", function(e) {
            e.target.value = '';
        });

        templateDropdown[i].addEventListener("change", function() {
            this.blur();
        });
    };

});

function formatState (state) {
    if (!state.id) {
        return state.text;
    }

    var $state = $(
      '<span>' + state.element.templateHTML + '</span>'
    );
    return $state;
  };


function getTemplates(type, category) {
    let templateSelect = document.getElementById(type+"Select");
    var data = null;
    if (data_raw.dataset.data.length > 0) {
        data = JSON.parse(data_raw.dataset.data);
    } else {
        console.log("Template data not found");
        return
    }
    
    templateSelect.options.length = 1;
    $('#'+type+'Select').val(null).trigger('change'); // clear existing selection
    // $('.'+ type + '-edit').html("");

    for( const [key, value] of Object.entries(data[type].list)){
        if(value.category === category) {
            let option = document.createElement("option");
            var template_text = value.text;
            var template_text_html = value.text;
            let xyz = ['x', 'y', 'z'];
            xyz.forEach(r => {
                if(template_text.includes('%' + r)) {
                    let replace_text = '';
                    let replace_text_html = '';
                    let examples = [];
                    if(value[r + '_type'] == 'dropdown'){
                        examples = value[r + '_values'].split(',');
                    } else {
                        examples = value[r + '_examples'].split(',');         
                    }
                    replace_text = examples[Math.floor(Math.random()*examples.length)].trim();
                    replace_text_html = "<span class='replace-text'>" + replace_text + "</span>";
                    template_text = template_text.replace('%' + r, replace_text);
                    template_text_html = template_text_html.replace('%' + r, replace_text_html);
                }
            });

            option.text = template_text;
            option.value = JSON.stringify(value);
            if (type == 'conditions') {
                option['templateHTML'] = 'If ' + template_text_html;
            } else {
                option['templateHTML'] = template_text_html;
            }
            
            templateSelect.add(option);
        }
    }
}

$( '#generateButton' ).on('click', function() {
    var ticket_data = getTicketData();
    if (ticket_data) {
        let parts = ['conditions', 'actions'];
        let xyz = ['x', 'y', 'z'];
        let plain_text = {conditions: '', actions: ''};
    
        // hide all the helpers now that a ticket has been made
        helperVisible = false;
        $('.helper').each(function() {
            $(this).hide();
        })
    
        parts.forEach( (p) => {
            plain_text[p] = ticket_data[p].text;
            xyz.forEach( v => {
                if (plain_text[p].includes('%' + v)){
                    plain_text[p] = plain_text[p].replace('%' + v, ticket_data[p][v + '_value'])
                }
            })
        });
    
        $( '#conditionText').text(strip(plain_text.conditions));
        $( '#actionText').text(strip(plain_text.actions));
        $( '#eventText').text(strip(ticket_data.event_type));
        $( '#saveButton').show();
        $( '#introButton').hide();
        closeEdit();
    } else {
        $( '#generateModal').modal('show');
    }



});

function getTicketData() {
    var error = false;
    var ticket_data = {
        name: null,
        description: null,
        event_type: $('#eventList').val(),
        metadata_json: null
    };

    var part_data_blank = {
        text: null,
        category: null,
        x_value: null,
        y_value: null,
        z_value: null,
        template_json: null,
        metadata_json: null
    }

    let parts = ['conditions', 'actions'];
    let xyz = ['x', 'y', 'z'];

    parts.forEach( (p) => {
        ticket_data[p] = JSON.parse(JSON.stringify(part_data_blank));
        let part_data_raw = $('#'+p+'Select').select2('data');
        if (part_data_raw.length > 0) {
            let part_data = JSON.parse(part_data_raw[0].id);
            ticket_data[p].text = part_data.text;
            ticket_data[p].category = part_data.category;
            ticket_data[p].template_json = JSON.stringify(part_data);
        } else {
            error = true;
        }
        xyz.forEach( v => {
            var v_value = $('#'+v+'Value-'+p).val();
            if (v_value !== undefined) {
                if (v_value == '') {error = true;}
                ticket_data[p][v+'_value'] = v_value;
            }
        })
    });

    return (error ? false : ticket_data);
}

$( '#saveButton' ).on('click', () => {
    $('#saveButton').hide()
    $( "#saveSpinner" ).show();

    var ticket_data = getTicketData();

    if (ticket_data == false) {
        $( "#saveButton" ).show();
        $( "#saveSpinner" ).hide();
        return
    }

    $.post( "/api/ticket", {data: JSON.stringify(ticket_data)}, function(data) {
            setTimeout(function(){
                $( "#saveSpinner" ).hide();
                $('#editButton').hide();
                $('.ticket-container').removeClass('display-flex');
                $('.ticket-container').addClass('display-none');
                $('.help-bar').removeClass('display-flex');
                $('.help-bar').addClass('display-none');
                $('.success-container').removeClass('display-none');
                $('.success-container').addClass('display-flex');
                $('#successMessage').removeClass('display-none');
                $('#successMessage').addClass('display-flex');
                $( '#againButton').show();
            }, 1000);

        })
        .fail(function(data) {
            console.log(data.responseJSON.message);
            $( "#saveSpinner" ).hide();
        }) 
})

$( '.category-icon').on('click', function() {
    categories[this.dataset.type] = this.value;
    $('#' + this.dataset.type + 'ContentEdit').show();
    $('#' + this.dataset.type + 'SelectDiv').show();
    $('#' + this.dataset.type + 'Clear').hide();
    $("#" + this.dataset.type + "Helper").hide(); 
    $('#' + this.dataset.type + 'Edit').hide();
    $("#" + this.dataset.type + "CategoryLabel").text(': '+ this.value);
    $("#" + this.dataset.type + "Description").text(this.dataset.description);
    getTemplates(this.dataset.type, this.value);

    $(".category-icon-" + this.dataset.type).each(function() {
        if($(this).hasClass('category-icon-selected')){
            $(this).removeClass('category-icon-selected')
            $(this).addClass('category-icon-unselected')
         }
    });
    $(this).removeClass('category-icon-unselected');
    $(this).addClass('category-icon-selected')
})

$( '#editButton').on('click', function() {
    openEdit();
})

$( '#againButton').on('click', function() {
    $('.ticket-container').removeClass('display-none');
    $('.ticket-container').addClass('display-flex');
    $('.help-bar').removeClass('display-none');
    $('.help-bar').addClass('display-flex');
    $('#successMessage').removeClass('display-flex');
    $('#successMessage').addClass('display-none');
    $(this).hide();
    openEdit();
})

function openEdit() {
    $('#closeButton').show();
    $('#generateButton').show();
    $('#introButton').show();
    $('#saveButton').hide();
    $('#editButton').hide();

    $(".ticket").each(function() {
        $(this).removeClass('ticket-main');
        $(this).addClass('ticket-main-edit');
    });
    $(".ticket-edit").each(function() {
        $(this).show();
    });
    $(".ticket-print").each(function() {
        $(this).hide();
    });
    $(".category-icon").each(function() {
        $(this).prop('disabled', false);
    });
    $(".category-helper").each(function() {
        if (categories[this.dataset.type] === '') {
            $(this).show();
            $('#' + this.dataset.type + 'ContentEdit').hide();
        } else {
            $(this).hide();
        }
    });
}

$( '#closeButton').on('click', closeEdit);

function closeEdit() {
    $('#closeButton').hide();
    $('#editButton').show();
    $('#generateButton').hide();

    $(".ticket").each(function() {
        $(this).removeClass('ticket-main-edit');
        $(this).addClass('ticket-main');
    });
    $(".ticket-edit").each(function() {
        $(this).hide();
    });
    $(".ticket-print").each(function() {
        $(this).show();
    });
    $(".category-icon").each(function() {
        $(this).prop('disabled', true);
    });
    $(".category-helper").each(function() {
        $(this).hide();
    });
}

$('.btn-clear').on('click', function() {
    $(this).hide();
    let template_type = this.id.replace('Clear', '');
    $('#'+template_type+'SelectDiv').show();
    $('#'+template_type+'Edit').hide();
})

$('#nextButton').on('click', function() {
    advanceIntro(1);
})

$('#backButton').on('click', function() {
    advanceIntro(-1);
})

function advanceIntro(n){
    window.scrollTo(0,0);
    let introScreenNum_next = (introScreenNum+n) < 1 ? 1 : (introScreenNum+n);

    if ($('#intro' + introScreenNum_next).length == 0) {
        localStorage.setItem("introSeen", true);
        endIntro();
    } else {
        $('#intro' + introScreenNum.toString()).fadeOut(300, function() {
            $('#intro' + introScreenNum_next.toString()).fadeIn(300);
            introScreenNum = introScreenNum_next;
            introScreenNum == 1 ? $('#backButton').hide() : $('#backButton').show();
        });
    }
}

$('#helpButton').on('click', function() {
    if (helperVisible){
        $('.helper').each(function() {
            $(this).hide();
        })
        $('#helperText').text('show helpers');
    } else {
        $('.helper').each(function() {
            $(this).show();
        })
        $('#helperText').text('hide helpers');
    }
    helperVisible = !helperVisible;

}) 

function endIntro() {
    $('#intro' + introScreenNum.toString()).fadeOut(300, function() {
        $('#backButton').hide();
        $('#nextButton').hide();
        $('#skipButton').hide();
        $('#generateButton').show();
        $('#introButton').show();
        $('.intro-container').removeClass('display-flex');
        $('.intro-container').addClass('display-none');
        $('.ticket-container').removeClass('display-none');
        $('.ticket-container').addClass('display-flex');
        $('.help-bar').removeClass('display-none');
        $('.help-bar').addClass('display-flex');
        openEdit();
    }); 
}

function startIntro() {
    $('#intro' + introScreenNum.toString()).show(0, function() {
        $('#nextButton').show();
        if (introSeen) {$('#skipButton').show();}
        $('#backButton').hide();
        $('#editButton').hide();
        $('#closeButton').hide();
        $('#generateButton').hide();
        $('#introButton').hide();
        $('.ticket-container').removeClass('display-flex');
        $('.ticket-container').addClass('display-none');
        $('.help-bar').removeClass('display-flex');
        $('.help-bar').addClass('display-none');
        $('.intro-container').removeClass('display-none');
        $('.intro-container').addClass('display-flex');
    }); 
}

if (introSeen){
    endIntro();
} else {
    startIntro()
}


$('#introButton').on('click', function() {
    introScreenNum = 1;
    startIntro();
}) 

$('#skipButton').on('click', function() {
    endIntro();
}) 