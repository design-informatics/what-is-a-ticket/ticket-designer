(function () {
  'use strict';
  window.addEventListener('load', function () {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function (form) {
      form.addEventListener('submit', function (event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        } else {
          event.preventDefault();
          $('#registerModal').modal('show')
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();

$('#registerConfirm').on('click' , () => {
  console.log('click')
  $.post("/fringe/mailing-list", $("#registerform").serialize(), function (data) {
    return data;
  }).then(data => {
    $('#registerModal').modal('hide');
    $('.register-form').hide()
    $('#registerSuccess').show()
  })
  .fail(data => {
    console.log(data);

  })
});

$('#howButton').on('click', () => {
  $('#collapseTwo').collapse('hide');
  $('#collapseThree').collapse('hide');
})

$('#whatButton').on('click', () => {
  $('#collapseOne').collapse('hide');
  $('#collapseThree').collapse('hide');
})

$('#whyButton').on('click', () => {
  $('#collapseOne').collapse('hide');
  $('#collapseTwo').collapse('hide');
})