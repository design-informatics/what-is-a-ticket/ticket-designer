$(document).ready(function() {
    if (!localStorage.getItem('banner_seen')) {
        $('#cookieModal').modal('show');
    }   
  });

$('#cookieClose').on('click', function() {
    localStorage.setItem('banner_seen', true);
})