provider "openstack" {
   cloud = "openstack" 
}

variable "ticket_designer" {
   type = map
}

resource "openstack_compute_instance_v2" "ticket_designer" {
  name            = "ticket-designer-server"
  image_id        = "0e935645-97c2-4013-8950-ba939638c2d9"
  flavor_id       = "3"
  key_pair        = "mykey"
  security_groups = ["default", "web"]
  user_data = templatefile("cloud-config-ticket-designer.yml", var.ticket_designer)  

  metadata = {
    this = "that"
  }

  network {
    name = "VM Network Public"
  }
}

resource "openstack_compute_floatingip_associate_v2" "ticket_designer_ip" {
   floating_ip = "129.215.193.173"
   instance_id = openstack_compute_instance_v2.ticket_designer.id
   fixed_ip    = openstack_compute_instance_v2.ticket_designer.network.0.fixed_ip_v4
}