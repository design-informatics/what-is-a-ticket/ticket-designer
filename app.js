var createError = require('http-errors');
var express = require('express');
var path = require('path');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
const helmet = require("helmet");
const redis = require('redis');
const rateLimit = require("express-rate-limit");
var session = require('express-session');
var compression = require('compression');

var publicRouter = require('./routes/public');
var authRouter = require('./routes/auth');
var accountRouter = require('./routes/account');
var apiRouter = require('./routes/api');
var adminRouter = require('./routes/admin');

var db = require('./db/models');
const dotenv = require('dotenv');
dotenv.config();

const adminUsers = ['chris.elsden@ed.ac.uk', 'e.morgan@ed.ac.uk'];

db.sequelize.authenticate()
  .then(async () => {
    console.log('Connection has been established successfully.');
    await db.Parts.drop();
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err);
  });

db.sequelize.sync({ alter: true })
  .then(() => {
    console.log("All models were synchronized successfully.");
  });

var app = express();

app.use(compression()); //Compress all routes

app.use(
  helmet({
    contentSecurityPolicy:{
      directives: {
        defaultSrc: ["'self'"],
        scriptSrc: ["'self'", "https://code.jquery.com", "https://cdn.jsdelivr.net"],
        imgSrc: ["'self'", "data:"],
        formAction: ["'self'"],
        styleSrc: ["'self'", "https://cdn.jsdelivr.net", "https://fonts.googleapis.com"],
        fontSrc: ["'self'", "https://fonts.gstatic.com"],
      }
    }
  })
);

app.set('trust proxy', 1) // trust first proxy

// set up session middleware
let RedisStore = require('connect-redis')(session);
let redisClient = redis.createClient({url: process.env.REDIS_URL});

var sess = {
  store: new RedisStore({ client: redisClient }),
  secret: 'clarky cat',
  name: 'appsession',
  resave: false,
  saveUninitialized: false,
  cookie: {}
}

// Use secure cookies. NOTE: also need to set 'proxy_set_header X-Forwarded-Proto $scheme;' in NGINX
if (app.get('env') === 'production') {
  sess.cookie.path = '/';
  sess.cookie.secure = true;
  sess.cookie.httpOnly = true;
} 

app.use(session(sess))

// set up rate limiter
const authLimiter = rateLimit({
  windowMs: 15 * 60 * 1000, // 15 minutes
  max: 30,
  skipSuccessfulRequests: true
});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', publicRouter);
app.use('/auth', authLimiter, authRouter);

// Check if authorised on this session
app.use(function (req, res, next) {
  res.locals.authorised = req.session.authorised || false;
  if (!res.locals.authorised) {res.render('pages/login', {isLoggedIn: false})}
  else { next() };
})

// Post-auth routes
app.use('/account', accountRouter);
app.use('/api', apiRouter);

app.use('/admin', function (req, res, next) {
  const reject = () => {
    res.setHeader('www-authenticate', 'Basic')
    res.sendStatus(401)
  }

  const authorization = req.headers.authorization

  if (!authorization) {
    return reject()
  }

  const [username, password] = Buffer.from(authorization.replace('Basic ', ''), 'base64').toString().split(':')

  if (!(username === process.env.ADMIN_USER && password === process.env.ADMIN_PASS) || !adminUsers.includes(req.session.email)) {
    return reject()
  }

  next()
}, adminRouter
);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
