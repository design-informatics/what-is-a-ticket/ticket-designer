<image src="public/images/icon.png" style="width: 100px; display: block; margin-left: auto; margin-right: auto; padding-bottom: 10px">

<div align="center"><h1>Ticket Designer App</h1></div>


## About
This Ticket Designer is being built as part of Chris Elsden's "What is a Ticket?" project.

## Development
The app is built with Node.js and [express](https://expressjs.com/). The app uses a PostgreSQL database for all app data, and uses Redis as a session store.
### Running the app

#### Option 1: Use Docker
The easiest way to run the app is to use Docker. The app includes Docker compose files for development and production.
- [Install Docker](https://docs.docker.com/get-docker/)
- In your terminal/console make sure you're inside the project folder.
- If running in production, you will need to have the following environment variables set up, replacing the values in {} with your own variables:
    ```bash
    NODE_ENV=production
    DATABASE_URL="postgresql://{POSTGRES_USER}:{POSTGRES_PASSWORD}@postgres:5432/{POSTGRES_DB}"
    REDIS_URL=redis://127.0.0.1:6379
    ADMIN_USER={ADMIN_USER}
    ADMIN_PASS={ADMIN_PASSWORD}
    TEST_USER={TEST_USER}
    TEST_PASS={TEST_PASSWORD}
    POSTGRES_USER={POSTGRES_USER}
    POSTGRES_DB={POSTGRES_DB}
    POSTGRES_PASSWORD={POSTGRES_PASSWORD}
    ```
- Run docker compose - `docker-compose up -d`. For production, use - `docker-compose -f docker-compose.yml -f docker-compose.prod.yml up -d`
- You should see something like this:
    ```bash
    Starting redis-server                ... done
    Starting app_web_1      ... done
    Starting app_postgres_1 ... done
    ```
- In a browser, go to [http://localhost:3000/](http://localhost:3000/)
- You should see a log in screen. If not, wait a few seconds and refresh.
- Log in with username `tester` and password `tester`.

#### Option 2: Set up PostgreSQL database and Redis and run with npm
- Install PostgreSQL
- Set up role and database (example for Mac):
    - Login to postgres interface: `psql postgres`
    - Create a new role: `CREATE ROLE tester WITH LOGIN PASSWORD 'tester';`
    - Allow role to create databases: `ALTER ROLE tester CREATEDB;`
    - Quit postgres: `\q`
    - Login as new user: `psql -d postgres -U tester`
    - Create database: `CREATE DATABASE blankapp;`
- Install Redis
- Create a `.env` file in the project root, with the following contents:
    ```
    DATABASE_URL=postgresql://tester:tester@localhost:5432/blankapp
    REDIS_URL=redis://127.0.0.1:6379
    ADMIN_USER=dev
    ADMIN_PASS=test
    TEST_USER=tester
    TEST_PASS=tick3t
    ```
- In your terminal/console make sure you're inside the project folder.
- Assuming you have installed Node, run:
    ```
    npm install
    npx sequelize-cli db:migrate
    npx sequelize-cli db:seed:all
    npm run watch
    ```
- In a browser, go to [http://localhost:3000/](http://localhost:3000/)   


## Useful Stuff
### Sequelize
The project uses [Sequelize](https://sequelize.org/) for database management. Use the Sequelize CLI to generate models and run [migrations](https://sequelize.org/master/manual/migrations.html).

e.g. `npx sequelize-cli model:generate --name Conditions --attributes time:date,user:string,text:string,category:string,x_value:string,y_value:string,z_value:string,template_json:string,metadata_json:string`

### Site Security
The site uses [Helmet](https://helmetjs.github.io/) to improve site security, by setting various HTTP headers. The site uses secure cookies for authentication. If serving the app using NGINX, you will need to add 'proxy_set_header X-Forwarded-Proto $scheme;' to the location block.