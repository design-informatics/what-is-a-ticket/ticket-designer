import pandas as pd
import os
dir_path = os.path.dirname(os.path.realpath(__file__))

import csv 
import json 

output = {}

def csv_to_json(csvFilePath):
    jsonArray = []
      
    #read csv file
    with open(csvFilePath, encoding='utf-8-sig') as csvf: 
        #load csv file data using csv library's dictionary reader
        csvReader = csv.DictReader(csvf) 

        #convert each csv row into python dict
        for row in csvReader:
            #add this python dict to json array
            if row['enabled'] == 'y':
                jsonArray.append(row)
            
  
    return jsonArray

          
conditionsFilePath = os.path.join(os.path.dirname(dir_path), 'data', 'conditions.csv')
actionsFilePath = os.path.join(os.path.dirname(dir_path), 'data', 'actions.csv')
jsonFilePath = os.path.join(os.path.dirname(dir_path), 'data', 'data.json')
json_conditions = csv_to_json(conditionsFilePath)
json_actions = csv_to_json(actionsFilePath)
output['conditions'] = {}
output['actions'] = {}
output['conditions']['list'] = json_conditions
output['actions']['list'] = json_actions

categories_conditions = []
categories_actions = []

for c in output['conditions']['list']:
    if c['category'] not in categories_conditions:
        categories_conditions.append(c['category'])

for c in output['actions']['list']:
    if c['category'] not in categories_actions:
        categories_actions.append(c['category'])        

output['conditions']['categories'] = categories_conditions
output['actions']['categories'] = categories_actions

with open(jsonFilePath, 'w', encoding='utf-8') as jsonf: 
    jsonString = json.dumps(output, indent=4)
    jsonf.write(jsonString)