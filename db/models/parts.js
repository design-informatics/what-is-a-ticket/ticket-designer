'use strict';
const Sequelize = require('sequelize');

const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Parts extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  Parts.init({
    ID: {
      type: DataTypes.UUID,
      defaultValue: Sequelize.UUIDV4
    },
    user: DataTypes.STRING,
    type: DataTypes.STRING,
    text: DataTypes.TEXT,
    category: DataTypes.STRING,
    x_value: DataTypes.STRING,
    y_value: DataTypes.STRING,
    z_value: DataTypes.STRING,
    template_json: DataTypes.TEXT,
    metadata_json: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Parts',
  });
  return Parts;
};