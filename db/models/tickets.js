'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Tickets extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  Tickets.init({
    user: DataTypes.STRING,
    condition_id: DataTypes.STRING,
    action_id: DataTypes.STRING,
    condition_text: DataTypes.TEXT,
    action_text: DataTypes.TEXT,
    condition_category: DataTypes.TEXT,
    action_category: DataTypes.TEXT,
    name: DataTypes.STRING,
    description: DataTypes.TEXT,
    event_type: DataTypes.TEXT,
    stars: DataTypes.INTEGER,
    metadata_json: DataTypes.TEXT,
    reported: DataTypes.BOOLEAN
  }, {
    sequelize,
    modelName: 'Tickets',
  });
  return Tickets;
};