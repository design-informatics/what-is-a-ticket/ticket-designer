const db = require("../db/models");
const bcrypt = require('bcryptjs');
const {
    v4: uuidv4
} = require('uuid');
const saltRounds = 10;
const Sequelize = require('sequelize');
const {
    Op
} = require("sequelize");

var Filter = require('bad-words'),
    filter = new Filter();

function moderate(text) {
    if (text != null) {
        text = filter.clean(text);
        text = text.replace(/<[^>]*>?/gm, '');
    }
    return text;
}

async function getUser(email, password) {
    var result = false;
    await db.Users.findAll({
            where: {
                email: email
            },
        }).then(async (entries) => {
            if (entries.length > 0) {
                await bcrypt.compare(password, entries[0].password)
                    .then((match) => {
                        if (match) {
                            delete entries[0].password; // don't want to return the password
                            result = entries[0];
                        }
                    })
                    .catch(err => {
                        result = false;
                    });
            }
        })
        .catch(err => {
            result = false;
        });

    return result;
}

async function createUser(username, email, password, admin, subscribe) {
    var result = false;

    // hash password with bcrypt
    await bcrypt.hash(password, saltRounds)
        .then(async (hash) => {
            const newuser = {
                username: username,
                password: hash,
                email: email,
                admin: admin,
                subscribe: subscribe
            };
            await db.Users.create(newuser)
                .then(data => {
                    result = data;
                })
                .catch(err => {
                    result = false;
                });
        })
        .catch(err => {
            result = false;
        });

    return result;
}

async function checkUserExists(email) {
    var result = false;

    await db.Users.findAll({
            where: {
                email: email
            }
        }).then((entries) => {
            if (entries.length != 0) {
                result = true;
            }
        })
        .catch(err => {
            result = false;
            console.log(err);
        });
    return result;
}

async function updateUser(email, username, password, admin) {
    var result = true;

    // hash password with bcrypt
    bcrypt.hash(password, saltRounds, async (err, hash) => {
        await db.Users.update({
                username: username,
                password: hash,
                admin: admin
            }, {
                where: {
                    email: email,
                }
            })
            .catch(err => {
                console.log(err);
                result = false;
            });
    });

    return result;
}

async function deleteUser(userID) {
    var result = [false, false, false];
    await db.Users.destroy({
            where: {
                id: userID.toString(),
            }
        })
        .then(data => {
            result[0] = data;
        })
        .catch(err => {
            result[0] = false;
        });

    await db.Tickets.destroy({
            where: {
                user: userID.toString(),
            }
        })
        .then(data => {
            result[1] = data;
        })
        .catch(err => {
            result[1] = false;
        });

    await db.Parts.destroy({
            where: {
                user: userID.toString(),
            }
        })
        .then(data => {
            result[2] = data;
        })
        .catch(err => {
            result[2] = false;
        });

    const notFalse = (currentValue) => currentValue !== false;

    return result.every(notFalse) ? true : false;

}


async function restoreTicket(ticket) {
    var result = false;
    var parts = ['condition', 'action'];

    parts.forEach(async p => {
        var str = ticket[p+'_text'];
        var xyz =  str.match(/<span [^>]+>(.*?)<\/span>/g).map(function(val){
            return val.replace(/<[^>]*>?/gm, '');
         });
        newpart = {
            ID: ticket[p+'_id'],
            user: ticket.user,
            type: p+'s',
            text: ticket[p+'_text'],
            category: ticket[p+'_category'],
            x_value: xyz[0] || null,
            y_value: xyz[1] || null,
            z_value: xyz[2] || null,
            template_json: null,
            metadata_json: null,
        };

        await db.Parts.create(newpart)
            .then(data => {

            })
            .catch(err => {
                result = false;
                console.log(err);
            });

    })

    const newticket = {
        user: ticket.user,
        condition_id: ticket.condition_id,
        action_id: ticket.action_id,
        condition_text: ticket.condition_text,
        action_text: ticket.action_text,
        condition_category: ticket.condition_category,
        action_category: ticket.action_category,
        name: ticket.name,
        description: ticket.description,
        event_type: ticket.event_type,
        stars: 0,
        metadata_json: ticket.metadata_json,
        reported: false,
    }

    await db.Tickets.create(newticket)
        .then(data => {
            result = data.dataValues;
        })
        .catch(err => {
            result = false;
            console.log(err);
        });

    return result;
}


async function addTicket(ticket_data, userID) {
    var result = false;
    var parts = ['conditions', 'actions'];
    var uuids = {
        conditions: null,
        actions: null
    };
    uuids.conditions = uuidv4();
    uuids.actions = uuidv4();
    var plaintext = {
        conditions: '',
        actions: ''
    };
    var xyz = ['x', 'y', 'z'];

    parts.forEach(async p => {
        let raw_text = moderate(ticket_data[p].text);
        xyz.forEach(t => {
            if (raw_text.includes('%' + t)) {
                raw_text = raw_text.replace('%' + t, '<span class="print-part">' + moderate(ticket_data[p][t + '_value']) + '</span>');
            }
        })
        plaintext[p] = raw_text;

        newpart = {
            ID: uuids[p],
            user: userID,
            type: p,
            text: ticket_data[p].text,
            category: ticket_data[p].category,
            x_value: moderate(ticket_data[p].x_value),
            y_value: moderate(ticket_data[p].y_value),
            z_value: moderate(ticket_data[p].z_value),
            template_json: ticket_data[p].template_json,
            metadata_json: ticket_data[p].metadata_json,
        };

        await db.Parts.create(newpart)
            .then(data => {

            })
            .catch(err => {
                result = false;
                console.log(err);
            });

    })

    const newticket = {
        user: userID,
        condition_id: uuids.conditions,
        action_id: uuids.actions,
        condition_text: plaintext.conditions,
        action_text: plaintext.actions,
        condition_category: ticket_data['conditions'].category,
        action_category: ticket_data['actions'].category,
        name: ticket_data.name,
        description: ticket_data.description,
        event_type: moderate(ticket_data.event_type),
        stars: 0,
        metadata_json: ticket_data.metadata_json,
        reported: false
    }

    await db.Tickets.create(newticket)
        .then(data => {
            result = data.dataValues;
        })
        .catch(err => {
            result = false;
            console.log(err);
        });

    return result;
}

async function deleteTicket(ID, userID) {
    var result = [false, false, false];

    await db.Tickets.findOne({
            where: {
                id: ID,
                user: userID.toString()
            },
            raw: true
        })
        .then(async (data) => {
            if (data != null) {
                result[0] = data;
                if (result[0].id == ID) {
                    await db.Tickets.destroy({
                            where: {
                                id: ID,
                            }
                        })
                        .then(ticket_data => {
                            result[1] = ticket_data;
                        })
                        .catch(err => {
                            result[1] = false;
                        });

                    await db.Parts.destroy({
                            where: {
                                ID: [data.condition_id, data.action_id]
                            }
                        })
                        .then(part_data => {
                            result[2] = part_data;
                        })
                        .catch(err => {
                            console.log(err)
                            result[2] = false;
                        });
                }
            }
        })
        .catch(err => {
            result[0] = false;
        });

    const notFalse = (currentValue) => currentValue !== false;

    return result.every(notFalse) ? true : false;

}

async function getParts() {
    var result = false;
    await db.Parts.findAll({
            where: {
                createdAt: {
                    [Op.gt]: new Date('2021-08-19T07:00:00')
                }
            },
            raw: true
        }).then((entries) => {
            result = entries;
        })
        .catch(err => {
            console.log(err);
        });
    return result;
}


async function getTickets(userID) {
    var result = false;

    await db.Tickets.findAll({
            where: {
                user: userID.toString()
            },
            order: [
                ['createdAt', 'DESC']
            ],
            raw: true,
        }).then((entries) => {
            if (entries[0] !== undefined) {
                result = entries;
            }
        })
        .catch(err => {
            console.log(err);
        });
    return result;
}

async function getTicket(ID) {
    var result = false;

    await db.Tickets.findAll({
            where: {
                id: parseInt(ID),
                reported: {
                    [Op.not]: true
                },
            },
            raw: true,
            attributes: {
                exclude: ['user']
            },
        }).then((entries) => {
            if (entries[0] !== undefined) {
                result = entries[0];
            }
        })
        .catch(err => {
            console.log(err);
        });
    return result;
}

async function getRandomTicket(seen) {
    var result = false;
    await db.Tickets.findAll({
            where: {
                id: {
                    [Op.notIn]: seen
                },
                reported: {
                    [Op.not]: true
                },
                createdAt: {
                    [Op.gt]: new Date('2021-08-19T07:00:00')
                }
            },
            order: Sequelize.literal('random()'),
            limit: 2,
            attributes: {
                exclude: ['user']
            },
            raw: true
        }).then((entries) => {
            if (entries[0] !== undefined) {
                result = entries;
            }
        })
        .catch(err => {
            console.log(err);
        });
    return result;
}

async function getAllTickets(exclude) {
    var result = false;
    await db.Tickets.findAll({
            where: {
                createdAt: {
                    [Op.gt]: new Date('2021-08-19T07:00:00')
                }
            },
            attributes: {
                exclude: exclude
            },
            raw: true
        }).then((entries) => {
            if (entries[0] !== undefined) {
                result = entries;
            }
        })
        .catch(err => {
            console.log(err);
        });
    return result;
}

async function reportTicket(ID) {
    var result = false;
    await db.Tickets.update({
            reported: true
        }, {
            where: {
                id: ID
            }
        }).then((entries) => {
            if (entries[0] !== undefined) {
                result = true;
            }
        })
        .catch(err => {
            console.log(err);
        });
    return result;
}


async function register(email) {
    var result = false;

    await db.Mailinglist.create({
            email: email
        })
        .then(data => {
            result = data;
        })
        .catch(err => {
            result = false;
        });

    return result;
}

async function getML() {
    var result = false;

    await db.Mailinglist.findAll({
            raw: true
        })
        .then((entries) => {
            if (entries.length != 0) {
                result = entries;
            }
        })
        .catch(err => {
            result = false;
            console.log(err);
        });
    return result;
}

async function getUsers() {
    var result = false;

    await db.Users.findAll({
            raw: true,
            attributes: {
                exclude: ['user', 'password']
            },
        })
        .then((entries) => {
            if (entries.length != 0) {
                result = entries;
            }
        })
        .catch(err => {
            result = false;
            console.log(err);
        });
    return result;
}

module.exports = {
    getUser: getUser,
    createUser: createUser,
    checkUserExists: checkUserExists,
    updateUser: updateUser,
    deleteUser: deleteUser,
    addTicket: addTicket,
    getParts: getParts,
    getTickets: getTickets,
    getTicket: getTicket,
    getRandomTicket: getRandomTicket,
    register: register,
    getML: getML,
    getUsers: getUsers,
    reportTicket: reportTicket,
    deleteTicket: deleteTicket,
    getAllTickets: getAllTickets,
    restoreTicket: restoreTicket
}