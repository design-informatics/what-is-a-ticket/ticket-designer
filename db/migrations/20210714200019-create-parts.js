'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Parts', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      ID: {
        type: Sequelize.UUID,
      },
      time: {
        type: Sequelize.DATE
      },
      user: {
        type: Sequelize.STRING
      },
      type: {
        type: Sequelize.STRING
      },
      text: {
        type: Sequelize.TEXT
      },
      category: {
        type: Sequelize.STRING
      },
      x_value: {
        type: Sequelize.STRING
      },
      y_value: {
        type: Sequelize.STRING
      },
      z_value: {
        type: Sequelize.STRING
      },
      template_json: {
        type: Sequelize.TEXT
      },
      metadata_json: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Parts');
  }
};