'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Tickets', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      time: {
        type: Sequelize.DATE
      },
      user: {
        type: Sequelize.STRING
      },
      condition_id: {
        type: Sequelize.STRING
      },
      action_id: {
        type: Sequelize.STRING
      },
      condition_text: {
        type: Sequelize.TEXT
      },
      action_text: {
        type: Sequelize.TEXT
      },
      condition_category: {
        type: Sequelize.TEXT
      },
      action_category: {
        type: Sequelize.TEXT
      },
      name: {
        type: Sequelize.STRING
      },
      description: {
        type: Sequelize.TEXT
      },
      event_type: {
        type: Sequelize.TEXT
      },
      stars: {
        type: Sequelize.INTEGER
      },
      metadata_json: {
        type: Sequelize.TEXT
      },
      reported: {
        type: Sequelize.BOOLEAN,
        defaultValue: false
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Tickets');
  }
};